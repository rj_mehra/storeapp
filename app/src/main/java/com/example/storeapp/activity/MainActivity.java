package com.example.storeapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import com.example.storeapp.R;
import com.example.storeapp.adapter.StoreAdapter;
import com.example.storeapp.databinding.ActivityMainBinding;
import com.example.storeapp.util.AnimationUtils;
import com.example.storeapp.util.GlobalApp;
import com.example.storeapp.viewmodel.MainActivityViewModel;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding activityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        activityMainBinding.rvStoreItem.setLayoutManager(linearLayoutManager);
        activityMainBinding.setMainViewModel(new MainActivityViewModel(this));
        activityMainBinding.executePendingBindings();
    }
}
