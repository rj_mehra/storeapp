package com.example.storeapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Explode;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;

import com.example.storeapp.R;
import com.example.storeapp.databinding.ActivityItemDetailsBinding;
import com.example.storeapp.model.StoreItem;
import com.example.storeapp.util.AnimationUtils;
import com.example.storeapp.viewmodel.ItemDetailViewModel;

public class ItemDetailsActivity extends AppCompatActivity implements ItemDetailViewModel.ItemDetailClickListener {
    private StoreItem storeItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if(intent!=null && intent.hasExtra("storeItems")){
            storeItem = (StoreItem)intent.getSerializableExtra("storeItems");
        }
        ActivityItemDetailsBinding activityItemDetailsBinding = DataBindingUtil.setContentView(this,R.layout.activity_item_details);
        activityItemDetailsBinding.setItemDetailViewModel(new ItemDetailViewModel(storeItem,this));
        activityItemDetailsBinding.executePendingBindings();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClose() {
        Intent intent = new Intent(ItemDetailsActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.bottom_up, R.anim.no_transition);
        finish();
    }
}
