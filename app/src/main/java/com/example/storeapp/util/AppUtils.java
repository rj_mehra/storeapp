package com.example.storeapp.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.appcompat.content.res.AppCompatResources;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;

public class AppUtils {

    public static void loadImage(final Context mContext, final int drawable, final ImageView imageView,
                                 int placeHolder) {
        try {
            if (mContext == null || imageView == null) return;
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(AppCompatResources.getDrawable(mContext, placeHolder));
            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
            requestOptions.error(AppCompatResources.getDrawable(mContext, placeHolder));
            Glide.with(mContext).load(drawable).apply(requestOptions).into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
