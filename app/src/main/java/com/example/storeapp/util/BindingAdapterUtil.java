package com.example.storeapp.util;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.storeapp.R;
import com.example.storeapp.adapter.StoreAdapter;

public class BindingAdapterUtil {

    @BindingAdapter({ "app:storeAdapter" })
    public static void setStoreAdapter(RecyclerView recyclerView,
                                                   StoreAdapter storeAdapter) {
        if (storeAdapter == null) return;
        recyclerView.setAdapter(storeAdapter);
    }

    @BindingAdapter({ "app:loadImage" })
    public static void loadGlideImage(ImageView imageView, int drawable) {
        AppUtils.loadImage(imageView.getContext(), drawable, imageView,
                R.drawable.ic_image_placeholder);
    }
}
