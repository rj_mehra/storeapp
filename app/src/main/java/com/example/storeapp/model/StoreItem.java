package com.example.storeapp.model;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class StoreItem implements Serializable {
    private String name, description;
    private int drawable;

    public StoreItem(String name, String description, int drawable) {
        this.name = name;
        this.description = description;
        this.drawable = drawable;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
