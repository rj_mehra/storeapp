package com.example.storeapp.viewmodel;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.storeapp.R;
import com.example.storeapp.activity.ItemDetailsActivity;
import com.example.storeapp.activity.MainActivity;
import com.example.storeapp.adapter.StoreAdapter;
import com.example.storeapp.model.StoreItem;
import com.example.storeapp.util.GlobalApp;

import java.util.ArrayList;

public class MainActivityViewModel implements StoreAdapter.ClickListener {
    public StoreAdapter storeAdapter;
    private AppCompatActivity context;
    ArrayList<StoreItem> storeItems = new ArrayList<>();
    public MainActivityViewModel(AppCompatActivity activity){
        this.context = activity;
        storeAdapter = new StoreAdapter(this);
        setRecyclerViewData();
    }

    private void setRecyclerViewData(){
        storeItems.add(new StoreItem("Alien vs Predator","A game about the best fight in the universe",R.drawable.app_sample_image_alien));
        storeItems.add(new StoreItem("Alita","A game about alyssa's adventure exploring the world",R.drawable.app_image_2));
        storeAdapter.notifyData(storeItems);
    }

    @Override
    public void onItemClick(StoreItem storeItem) {
        final View androidRobotView = context.findViewById(R.id.rv_storeItem);

        if(context == null) return;
//        sharedElementTransition();
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(context,androidRobotView,"robot");
        Intent intent = new Intent(context, ItemDetailsActivity.class);
        intent.putExtra("storeItems", storeItem);
        context.startActivity(intent,options.toBundle());
    }

    public void sharedElementTransition() {
        final RecyclerView storeItemView = (RecyclerView) context.findViewById(R.id.rv_storeItem);
        final ImageView imageView = (ImageView) context.findViewById(R.id.imageView);
        final AppCompatTextView textView3 = (AppCompatTextView)context.findViewById(R.id.textView3);
        final AppCompatTextView button = (AppCompatTextView) context.findViewById(R.id.button);
        android.util.Pair[] pair = new android.util.Pair[4];
        pair[0] = new android.util.Pair<View, String>(storeItemView, "rv_storeItem");
        pair[1] = new android.util.Pair<View, String>(imageView, "imageView");
        pair[2] = new Pair<View, String>(textView3, "textView3");
        pair[3] = new Pair<View,String>(button,"button");

        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(context, pair);
        Intent i = new Intent(context, ItemDetailsActivity.class);
        context.startActivity(i, options.toBundle());
    }
}
