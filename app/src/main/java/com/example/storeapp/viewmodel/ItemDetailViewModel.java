package com.example.storeapp.viewmodel;

import com.example.storeapp.model.StoreItem;

public class ItemDetailViewModel {
    public StoreItem storeItem;
    private ItemDetailClickListener itemDetailClickListener;

    public ItemDetailViewModel(StoreItem storeItem, ItemDetailClickListener itemDetailClickListener){
        this.storeItem = storeItem;
        this.itemDetailClickListener = itemDetailClickListener;
    }

    public void onDownloadClick(){

    }

    public void onCancelClick(){

    }

    public void onCloseClick(){
        itemDetailClickListener.onClose();
    }

    public interface ItemDetailClickListener{
        void onClose();
    }
}
