package com.example.storeapp.viewmodel;

import android.content.Intent;

import com.example.storeapp.adapter.StoreAdapter.ClickListener;
import com.example.storeapp.model.StoreItem;

public class StoreAdapterViewModel {
    public StoreItem storeItem;
    public ClickListener clickListener;

    public StoreAdapterViewModel(StoreItem storeItem, ClickListener clickListener){
        this.storeItem = storeItem;
        this.clickListener =clickListener;
    }

    public void onItemClick(){
        clickListener.onItemClick(storeItem);
    }
}
