package com.example.storeapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.storeapp.R;
import com.example.storeapp.databinding.StoreItemBinding;
import com.example.storeapp.model.StoreItem;
import com.example.storeapp.viewmodel.StoreAdapterViewModel;

import java.util.ArrayList;

public class StoreAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<StoreItem> storeItemArrayList = new ArrayList<>();
    private ClickListener clickListener;

    public StoreAdapter(ClickListener clickListener){
        this.clickListener = clickListener;
    }

    public void notifyData(ArrayList<StoreItem> storeItems) {
        this.storeItemArrayList.clear();
        this.storeItemArrayList.addAll(storeItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.store_item, parent, false);
        StoreItemBinding storeItemBinding = DataBindingUtil.bind(view);
        return new StoreViewHolder(storeItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((StoreViewHolder) holder).onBind(position);
    }

    @Override
    public int getItemCount() {
        return storeItemArrayList.size();
    }

    private class StoreViewHolder extends RecyclerView.ViewHolder {
        private StoreItemBinding storeItemBinding;
        private StoreAdapterViewModel storeAdapterViewModel;
        private StoreViewHolder(@NonNull StoreItemBinding itemView) {
            super(itemView.getRoot());
            storeItemBinding = itemView;
            this.storeItemBinding.executePendingBindings();
        }

        private void onBind(int position) {
            storeAdapterViewModel = new StoreAdapterViewModel(storeItemArrayList.get(position), clickListener);
            storeItemBinding.setStoreItemViewModel(storeAdapterViewModel);
        }
    }

    public interface ClickListener{
        void onItemClick(StoreItem storeItem);
    }
}
